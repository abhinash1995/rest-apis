package com.abhinash.restapi.util;

public class CommonUtil {

    /**
     * Method to test whether the given list is empty or not.
     *
     * @param inputList
     * @return
     */
    @SuppressWarnings("rawtypes")
    public static boolean isEmpty(String... inputList) {
        for (String s : inputList) {
            if (s != null) {
                return false;
            }
        }
        return true;
    }
}
