package com.abhinash.restapi.util;

import com.abhinash.restapi.service.PersonService;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

@Component
public class PDFGenerator {

    @Autowired
    PersonService personService;

    public static final String TEMP_PDF_FOLDER = "/home/mmfpcdev1/myproj/voucherpdf";
    public static final String GENERATED_PDF_FILE_NAME = TEMP_PDF_FOLDER + "/voucher";

    public void getPersonDetails(String filePath) {
        Document document = new Document();


        try {
//            document.setPageSize(PageSize.A4);
//            document.setMargins(36, 36, 36, 100);
//            document.setMarginMirroring(true);
            PdfWriter.getInstance(document, new FileOutputStream(filePath));
            document.open();
            document.add(generateTable());
            document.close();

        } catch (DocumentException | FileNotFoundException e) {
            e.printStackTrace();
        }


    }

    private PdfPTable generateTable() {
        PdfPTable table = new PdfPTable(2);
        PdfPCell cell;
        cell = new PdfPCell(new Phrase("Flight Itenary"));
        cell.setColspan(2);//to take full column
        table.addCell(cell);
//to take full column
        cell = new PdfPCell(new Phrase("Flight Details"));
        cell.setColspan(2);
        table.addCell(cell);

        table.addCell("Airlines");
        table.addCell("Yati");

        table.addCell("Departure City");
        table.addCell("Pokhara");

        table.addCell("Arrival City");
        table.addCell("KathMandu");

        table.addCell("Flight No ");
        table.addCell("EK12345");

        table.addCell("Departure Date");
        table.addCell("2/33/44");

        table.addCell("Departure Time");
        table.addCell("12 PM");

        cell = new PdfPCell(new Phrase("Passenger Details"));
        cell.setColspan(2);
        table.addCell(cell);

        table.addCell("First Name");
        table.addCell("Abhinash Kumar Pandey");

        table.addCell("Last Name");
        table.addCell("Pandey");

        table.addCell("Email");
        table.addCell("abhinash7643@gmail.com");

        table.addCell("Phone");
        table.addCell("+919538653598");

        return table;

    }

}