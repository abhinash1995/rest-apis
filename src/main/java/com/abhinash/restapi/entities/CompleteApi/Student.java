package com.abhinash.restapi.entities.CompleteApi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.vladmihalcea.hibernate.type.array.StringArrayType;
import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import jdk.nashorn.internal.ir.ObjectNode;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;

@Entity
@Table(name = "student")
@TypeDefs({ @TypeDef(name = "Json", typeClass = JsonBinaryType.class),
        @TypeDef(name = "StringArray", typeClass = StringArrayType.class) })
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long sId;

    @Column(name = "student_name")
    private String sName;

    @Column(name = "date_of_birth")
    private String dob;

    @Column(name = "gender")
    private char gender;

    @Column(name = "subjects")
    private String[] subjects;

    @Column(name = "aadhar")
    private ObjectNode aadhar;

    @ManyToOne
    @JoinColumn(name = "dept_id")
    private Department department;

    public Long getsId() {
        return sId;
    }

    public void setsId(Long sId) {
        this.sId = sId;
    }

    public String getsName() {
        return sName;
    }

    public void setsName(String sName) {
        this.sName = sName;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public char getGender() {
        return gender;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }

    public String[] getSubjects() {
        return subjects;
    }

    public void setSubjects(String[] subjects) {
        this.subjects = subjects;
    }

    public ObjectNode getAadhar() {
        return aadhar;
    }

    public void setAadhar(ObjectNode aadhar) {
        this.aadhar = aadhar;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }
}
