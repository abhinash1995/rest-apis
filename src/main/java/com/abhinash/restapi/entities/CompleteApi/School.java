package com.abhinash.restapi.entities.CompleteApi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import javax.persistence.*;

@Entity
@Table(name = "school")
@TypeDef(name = "Json", typeClass = JsonBinaryType.class)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class School {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long Id;

    @Column(name = "school_name")
    private String sName;

    @Column(name = "affilatedWith")
    private String university;

    @Column(name = "school_type")
    private String sType;

    @Type(type = "Json")
    @Column(name = "school_address")
    private ObjectNode sAddress;

    @OneToOne
    @JoinColumn(name = "principle_id")
    private Principle principle;

//    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "school")
//    private List<Department> department;


    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getsName() {
        return sName;
    }

    public void setsName(String sName) {
        this.sName = sName;
    }

    public String getUniversity() {
        return university;
    }

    public void setUniversity(String university) {
        this.university = university;
    }

    public String getsType() {
        return sType;
    }

    public void setsType(String sType) {
        this.sType = sType;
    }

    public ObjectNode getsAddress() {
        return sAddress;
    }

    public void setsAddress(ObjectNode sAddress) {
        this.sAddress = sAddress;
    }

    public Principle getPrinciple() {
        return principle;
    }

    public void setPrinciple(Principle principle) {
        this.principle = principle;
    }

//    public List<Department> getDepartment() {
//        return department;
//    }
//
//    public void setDepartment(List<Department> department) {
//        this.department = department;
//    }
}
