package com.abhinash.restapi.entities.CompleteApi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.node.JsonNodeType;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import javax.persistence.*;


@Entity
@Table(name = "principle")
@TypeDef(name = "Json", typeClass = JsonBinaryType.class)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Principle {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long pId;

    @Column(name = "principle_name")
    private String dName;

    @Type(type = "Json")
    @Column(name = "principle_address")
    private ObjectNode sAddress;

    public Long getpId() {
        return pId;
    }

    public void setpId(Long pId) {
        this.pId = pId;
    }

    public String getdName() {
        return dName;
    }

    public void setdName(String dName) {
        this.dName = dName;
    }

    public ObjectNode getsAddress() {
        return sAddress;
    }

    public void setsAddress(ObjectNode sAddress) {
        this.sAddress = sAddress;
    }
}
