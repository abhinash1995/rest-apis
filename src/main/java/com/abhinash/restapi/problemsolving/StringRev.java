package com.abhinash.restapi.problemsolving;

import org.aspectj.weaver.World;

public class StringRev {

    public static void main(String[] args) {
        String name = "Abhinash";
        String revName = "";

        for(int i = name.length() - 1; i >= 0; i--)
        {
            revName = revName + name.charAt(i);
        }

        System.out.println("string Rev -> " + revName);

        String word ="I Love My Country";
        String[] words = word.split(" ");
        String revWord = "";
        for (int i = 0; i < words.length  ; i++) {
            if(i== words.length-1)
            revWord = words[i] + revWord;
            else
                revWord = " " + words[i] + revWord;
        }

        System.out.println("Reverse Word -> " + revWord);
        System.out.println("No of Words -> " + words.length);


        String str = "Hello World";
        String[] temp = str.split(" ");
        String reverseEachWordInString = "";
        for (int i = 0; i <temp.length ; i++) {
            String reveach = temp[i];
            String reverseWord = "";
            for(int j = reveach.length() - 1; i >= 0; i--)
            {
                reverseWord = reverseWord + reveach.charAt(j);
            }
            reverseEachWordInString = reverseEachWordInString + reverseWord + " ";
        }

        System.out.println(reverseEachWordInString);
    }

//    String[] words =  "This is interview question".split(" ");
//
//    String rev = "";
//for(int i = words.length - 1; i >= 0 ; i--)
//    {
//        rev += words[i] + " ";
//    }

//    Abhinash                     -> hsanihba
//    I Love My Country             -> Country My Love I    reverse words of a string
//    Hello Welcome To World       -> olleh emoclew ot dlrow    reverse each word
//    Hello Welcome To World       -> No of Word 4

}
