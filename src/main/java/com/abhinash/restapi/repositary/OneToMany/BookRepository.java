package com.abhinash.restapi.repositary.OneToMany;

import com.abhinash.restapi.entities.OneToMany.Book;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookRepository extends JpaRepository<Book, Long> {
}
