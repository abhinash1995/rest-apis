package com.abhinash.restapi.repositary.OneToMany;

import com.abhinash.restapi.entities.OneToMany.BookCategory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookCategoryRepository extends JpaRepository<BookCategory, Long> {
}
