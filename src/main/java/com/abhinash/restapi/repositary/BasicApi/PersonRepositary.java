package com.abhinash.restapi.repositary.BasicApi;

import com.abhinash.restapi.entities.BasicApi.Person;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PersonRepositary extends JpaRepository<Person, Long> {

    List<Person> getByBranch(String branch);

    List<Person> getByBranchAndLastName(String branch, String lastName);



}
