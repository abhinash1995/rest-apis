package com.abhinash.restapi.repositary.OneToOne;

import com.abhinash.restapi.entities.OneToOne.Laptop;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LaptopRepositary extends JpaRepository<Laptop, Long> {
}
