package com.abhinash.restapi.repositary.OneToOne;

import com.abhinash.restapi.entities.OneToOne.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeRepositary extends JpaRepository<Employee, Long> {
}
