package com.abhinash.restapi.repositary.ManyToMany;

import com.abhinash.restapi.entities.ManyToMany.Category;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<Category , Long> {

    Category findByName(String name);
}
