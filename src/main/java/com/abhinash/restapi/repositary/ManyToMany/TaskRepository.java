package com.abhinash.restapi.repositary.ManyToMany;

import com.abhinash.restapi.entities.ManyToMany.Task;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TaskRepository  extends JpaRepository<Task, Long> {

    List<Task> findByUserId(Long userId);
}
