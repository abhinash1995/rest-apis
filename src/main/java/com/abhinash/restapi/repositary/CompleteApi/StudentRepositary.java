package com.abhinash.restapi.repositary.CompleteApi;

import com.abhinash.restapi.entities.CompleteApi.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepositary extends JpaRepository<Student, Long> {
}
