package com.abhinash.restapi.repositary.CompleteApi;

import com.abhinash.restapi.entities.CompleteApi.Principle;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PrincipleRepositary extends JpaRepository<Principle, Long> {

}
