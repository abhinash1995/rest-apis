package com.abhinash.restapi.repositary.CompleteApi;

import com.abhinash.restapi.entities.CompleteApi.School;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SchoolRepositary extends JpaRepository<School, Long> {

}
