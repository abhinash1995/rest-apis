package com.abhinash.restapi.repositary.CompleteApi;

import com.abhinash.restapi.entities.CompleteApi.Department;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DepartmentRepository extends JpaRepository<Department , Long> {
}
