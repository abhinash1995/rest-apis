package com.abhinash.restapi.controller.OneToOne;

import com.abhinash.restapi.entities.BasicApi.Person;
import com.abhinash.restapi.entities.OneToOne.Employee;
import com.abhinash.restapi.repositary.OneToOne.EmployeeRepositary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/employee")
public class EmployeeController {

    @Autowired
    private EmployeeRepositary employeeRepositary;

    @PostMapping("/addEmployee")
    public Employee saveEmployee(@Valid @RequestBody Employee employee){
        return employeeRepositary.save(employee);
    }

    @GetMapping("/getAllEmployee")
    public List<Employee> getAllEmployee(){
       return employeeRepositary.findAll();
    }

    @GetMapping("getEmployee/{employeeId}")
    public Employee getEmployeeById(@PathVariable(value = "employeeId") Long id){
        return employeeRepositary.getOne(id);
    }

    @DeleteMapping("/deleteByEmployeeId/{employeeId}")
    public void deleteById(@PathVariable(value = "employeeId") Long id ){
        employeeRepositary.deleteById(id);
    }















    // using Mapstruct



}
