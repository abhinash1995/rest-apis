package com.abhinash.restapi.controller.CompleteApi;

import com.abhinash.restapi.entities.CompleteApi.School;
import com.abhinash.restapi.service.SchoolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/school")
public class SchoolController {

    @Autowired
    SchoolService schoolService;

    @PostMapping("/addSchool")
    public School savePerson(@Valid @RequestBody School school){
        return schoolService.saveSchool(school);
    }

}
