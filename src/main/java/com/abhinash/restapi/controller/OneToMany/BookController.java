package com.abhinash.restapi.controller.OneToMany;

import com.abhinash.restapi.entities.OneToMany.Book;
import com.abhinash.restapi.entities.OneToMany.BookCategory;
import com.abhinash.restapi.repositary.OneToMany.BookCategoryRepository;
import com.abhinash.restapi.repositary.OneToMany.BookRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/bookCategory")
public class BookController {

    Logger logger = LoggerFactory.getLogger(BookController.class);

    @Autowired
    BookCategoryRepository bookCategoryRepository;

    @Autowired
    BookRepository bookRepository;

    @PostMapping("/addBookCategory")
    public BookCategory savePerson(@Valid @RequestBody BookCategory bookCategory){
        bookCategory.getBooks().forEach(book -> book.setBookCategory(bookCategory));
        return bookCategoryRepository.save(bookCategory);
    }



//    @PostMapping("/addBookHard")
//    public BookCategory savePerson(){
//        Book b1 =  new Book("Hello Koding 1");
//        Book b2 =new Book("Hello Koding 2");
//        Set<Book>  books = new HashSet<>();
//        books.add(b1);
//        books.add(b2);
//      return bookCategoryRepository.save(new BookCategory("Category 1", books));
//    }



    @PostMapping("/addBook")
    public Book savePerson(@Valid @RequestBody Book book){
        return bookRepository.save(book);
    }


}
