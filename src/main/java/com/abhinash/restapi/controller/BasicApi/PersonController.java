package com.abhinash.restapi.controller.BasicApi;

import com.abhinash.restapi.entities.BasicApi.Person;
import com.abhinash.restapi.service.PersonService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/person")
public class PersonController {

    @Autowired
    private PersonService personService;

    @PostMapping("/addPersonDetails")
    public Person savePerson(@Valid @RequestBody Person person){
        return personService.savePerson(person);
    }

    @GetMapping("/getAllPerson")
    @ApiOperation(value = "getAllPerson",notes = "It will fetch all the person details from person table", response = Person.class)
    public List<Person> getAllPerson() {
        return personService.getAllPerson();

    }

    @GetMapping("/getAllPersonInEmail")
    public List<Person> sendAllPersonInEmail() {
        return personService.sendAllPersonDetailInEmail();

    }

    @DeleteMapping("/deleteById/{id}")
    public void deleteById(@PathVariable(value = "id") Long id ){
         personService.deletePerson(id);
    }

    @PutMapping("/update")
    public Person updatePerson(@RequestBody Person person){
        return personService.updatePerson(person);
    }

    @PostMapping("/saveAllPerson")
    public List<Person> saveAllPerson(@Valid @RequestBody List<Person> personList){
        return personService.saveAllPerson(personList);
    }

    @GetMapping("/getPersonByBranch/{branch}")
    public List<Person> getPersonByBranch( @PathVariable("branch") String branch) {
        return personService.getByBranch(branch);
    }

    @GetMapping("/getByBranchAndLastName/{branch}/{lastName}")
    public List<Person> getByBranchAndLastName( @PathVariable("branch") String branch, @PathVariable("lastName") String lastName) {
        return personService.getByBranchAndLastName(branch, lastName);
    }


}
