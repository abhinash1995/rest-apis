package com.abhinash.restapi.service;

import com.abhinash.restapi.entities.CompleteApi.School;
import com.abhinash.restapi.repositary.CompleteApi.SchoolRepositary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SchoolService {

    @Autowired
    SchoolRepositary schoolRepositary;

    public School saveSchool(School school){
     return schoolRepositary.save(school);
    }


}

