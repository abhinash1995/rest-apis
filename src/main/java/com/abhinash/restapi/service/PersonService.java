package com.abhinash.restapi.service;

import com.abhinash.restapi.entities.BasicApi.Person;
import com.abhinash.restapi.repositary.BasicApi.PersonRepositary;
import com.abhinash.restapi.util.EmailUtil;
import com.abhinash.restapi.util.PDFGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import java.util.List;

import static com.abhinash.restapi.util.PDFGenerator.GENERATED_PDF_FILE_NAME;

@Service
public class PersonService {


    @Autowired(required = true)
   PDFGenerator pdfGenerator;

    @Autowired
    EmailUtil emailUtil;

    @Autowired
    com.abhinash.restapi.Email.EmailUtil bestEmailUtil;

    @Autowired
    PersonRepositary personRepositary;

    //to save the person to Db
    public Person savePerson(Person d) {
        return personRepositary.save(d);
    }

    //to save the person to Db
    public List<Person> saveAllPerson(List<Person> personList) {
        return personRepositary.saveAll(personList);
    }


    //get all Person from Db
    public List<Person> getAllPerson() {
        return personRepositary.findAll();
    }

    //get all Person from Db
    public List<Person> sendAllPersonDetailInEmail() {
        String filePath = "/home/mmfpcdev1/myproj/voucherpdf/abc" + ".pdf";
        pdfGenerator.getPersonDetails(filePath);
//        emailUtil.sendPersonDetails("kumarabhinash7643@gmail.com", filePath);
        try {
            bestEmailUtil.sendAttachmentsMail("abhinash7643@gmail.com","Flight Itenary","This is Itenary",filePath,"kumarabhinash7643@gmail.com");
        } catch (MessagingException e) {
            e.printStackTrace();
        }
        return personRepositary.findAll();
    }

    //get Person by id
    public Person getPersonById(long id) {
        return personRepositary.getOne(id);
    }

    //update person
    public Person updatePerson(Person person) {
        return personRepositary.save(person);
    }

    //delete person from Db
    public void deletePerson(Long id) {
        personRepositary.deleteById(id);
    }

    public List<Person> getByBranch(String branch){
        return personRepositary.getByBranch(branch);
    }

    public List<Person> getByBranchAndLastName(String branch, String lastName){
        return personRepositary.getByBranchAndLastName(branch,lastName);
    }


}
