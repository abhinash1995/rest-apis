package com.abhinash.restapi.Email;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.MessagingException;
import javax.validation.Valid;

@RestController
public class EmailController {

    @Autowired
    private EmailUtil emailUtil;

    @Autowired
    private TemplateEngine templateEngine;

    @Autowired
    private ApplicationContext context;

    @RequestMapping(value = "/sendSimpleMail")
    public String sendEmail() {
        emailUtil.sendSimpleMail("kumarabhinash7643@gmail.com", "Test", "Hi Hello How Are You", "abhinash7643@gmail.com");
        return "Sent";
    }


    @PostMapping(value = "/sendHtmlMail")
    public String sendHtmlMail(@Valid @RequestBody EmailTest emailTest) throws MessagingException {
        Context context = new Context();
        context.setVariable("name", emailTest.getName());
        context.setVariable("cource", emailTest.getCource());
        String emailTemplate = templateEngine.process("welcome", context);
        emailUtil.sendHtmlMail("kumarabhinash7643@gmail.com", "Welcome Email", emailTemplate);
        return "Sent";
    }

    @PostMapping(value = "/sendEmailWithAttachment")
    public String sendPdfInEmail(@Valid @RequestBody EmailTest emailTest) throws MessagingException {
        Context context = new Context();
        context.setVariable("name", emailTest.getName());
        context.setVariable("cource", emailTest.getCource());
        String emailTemplate = templateEngine.process("welcome", context);
        String filePath = "/home/mmfpcdev1/myproj/voucherpdf/abc" + ".pdf";
        try {
            emailUtil.sendAttachmentsMail("abhinash7643@gmail.com", "Flight Itenary", emailTemplate, filePath, "kumarabhinash7643@gmail.com");
        } catch (MessagingException e) {
            e.printStackTrace();
        }
        return "Sent";
    }


}
