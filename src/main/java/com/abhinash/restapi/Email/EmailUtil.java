package com.abhinash.restapi.Email;

import org.springframework.stereotype.Component;

import javax.mail.MessagingException;

@Component
public interface EmailUtil {


    /**
     *
     * @param to receiver's address
     * @param subject Email subject
     * @param content content of email
     * @param cc CC address
     */
    void sendSimpleMail(String to, String subject, String content, String... cc);


    /**
     *
     * @param to   receiver's address
     * @param subject mail subject
     * @param content  content of email
     * @param cc CC address
     * @throws MessagingException Mail sent abnormally
     */
    void sendHtmlMail(String to, String subject, String content, String... cc) throws MessagingException;

    /**
     *
     * @param to   receiver's address
     * @param subject mail subject
     * @param content  content of email
     * @param filePath Attachment address
     * @param cc CC address
     * @throws MessagingException Mail sent abnormally
     *
     */
    void sendAttachmentsMail(String to, String subject, String content, String filePath, String... cc) throws MessagingException;


    /**
     *
     * @param to receiver's address
     * @param subject mail subject
     * @param content content of email
     * @param rscPath Static resource address
     * @param rscId  Static resource id
     * @param cc CC address
     * @throws MessagingException Mail sent abnormally
     */
    void sendResourceMail(String to, String subject, String content, String rscPath, String rscId, String... cc) throws MessagingException;
}
